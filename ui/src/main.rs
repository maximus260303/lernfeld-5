use axum::{AddExtensionLayer, Router};
use sqlx::{mysql::MySqlPoolOptions, MySqlPool};
use std::{process::exit, sync::Arc, time::Duration};
mod adresse;
mod bestellung;
mod kunde;

struct State {
    pub db_pool: MySqlPool,
}

#[tokio::main]
async fn main() {
    // set up logging
    tracing_subscriber::fmt::init();

    tracing::info!("Trying to connect to database");

    // set up database
    let maybe_db_pool = MySqlPoolOptions::new()
        .connect_timeout(Duration::from_secs(5))
        .connect("mysql://norom:fuck@localhost/kraut_und_rueben")
        .await;

    let db_pool = match maybe_db_pool {
        Ok(pool) => pool,
        Err(e) => {
            tracing::error!("Can not connect to database: {}", e);
            exit(-1);
        }
    };

    // set up state. used for requests to access the database
    let state = Arc::new(State { db_pool });

    // build our application
    let app = Router::new()
        .nest("/kunde", kunde::api())
        .nest("/bestellung", bestellung::api())
        .nest("/adresse", adresse::api())
        .layer(AddExtensionLayer::new(state));

    // run it with hyper on localhost:3000
    if let Err(e) = axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
        .serve(app.into_make_service())
        .await
    {
        tracing::error!("Can not start server: {}", e);
    }
}
