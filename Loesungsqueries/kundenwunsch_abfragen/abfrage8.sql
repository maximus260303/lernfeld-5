-- Auswahl aller Rezepte, die weniger als fünf Zutaten enthalten und eine bestimmte Ernährungskategorie erfüllen
SELECT * FROM
(SELECT r.name, COUNT(r.id) AS c  FROM rezept AS r
JOIN rezept_zutat AS rz
ON r.id = rz.rezept_id
JOIN zutat AS z
ON z.id = rz.zutat_id
JOIN rezept_ernaehrungskategorie AS re
ON re.rezept_id = r.id
JOIN ernaehrungskategorie AS e
ON e.id = re.ernaehrungskategorie_id
-- in der inneren Query Ernährungskat.wählen
WHERE e.name = "Vegan"
GROUP BY r.id) q1
WHERE c > 4
;