-- Berechnung der durchschnittlichen Nährwerte aller Bestellungen eines Kunden
SELECT k.name, k.vorname, AVG(z.kalorien)  FROM kunde AS k
JOIN bestellung AS b
ON k.id = b.kunde_id 
JOIN bestellung_rezept AS br
ON br.bestellung_id = b.id
JOIN rezept AS r
ON r.id = br.rezept_id
JOIN rezept_zutat AS rz
ON r.id = rz.rezept_id
JOIN zutat AS z
ON z.id = rz.zutat_id
WHERE k.name = "Maurer"
GROUP BY b.kunde_id
;