-- Auswahl aller Rezepte, die eine bestimmte Kalorienmenge nicht überschreiten

SELECT * FROM
(SELECT r.name, SUM(z.kalorien * rz.menge) AS sumkalorien  FROM rezept AS r
JOIN rezept_zutat AS rz
ON r.id = rz.rezept_id
JOIN zutat AS z
ON z.id = rz.zutat_id
GROUP BY r.id) kalorien
WHERE sumkalorien < 20000
;