-- delete user information (DSGVO) 

SET SQL_SAFE_UPDATES = 0;
DELETE FROM bestellung WHERE kunde_id = 4;
DELETE FROM adresse WHERE id = 4;
DELETE FROM kunde WHERE id = 4;
SET SQL_SAFE_UPDATES = 1;
