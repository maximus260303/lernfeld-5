-- LEFT JOIN

SELECT 
kunde.id AS KundenID,
kunde.name AS Nachname,
kunde.vorname AS Vorname,
kunde.geburtsdatum AS Geburtsdatum,
kunde.telefon AS TelefonNr,
kunde.email AS EMail,
bestellung.id AS BestellID 
FROM kunde
LEFT JOIN bestellung on bestellung.kunde_id = kunde.id
WHERE kunde.name = 'Müller';

-- RIGHT JOIN 

SELECT 
kunde.id AS KundenID,
bestellung.id AS BestellID,
bestellung.gesamtpreis AS Gesamtpreis,
bestellung.datum AS Bestelldatum
FROM kunde
RIGHT JOIN bestellung on bestellung.kunde_id = kunde.id
WHERE kunde.name = 'Foede';

-- INNER JOIN / INNER JOIN is the default value for JOIN

SELECT 
kunde.id AS KundenID,
kunde.name AS Nachname,
kunde.vorname AS Vorname,
kunde.geburtsdatum AS Geburtsdatum,
kunde.telefon AS TelefonNr,
kunde.email AS EMail,
bestellung.id AS BestellID,
bestellung.gesamtpreis AS Gesamtpreis,
bestellung.datum AS Bestelldatum
FROM kunde
INNER JOIN bestellung on bestellung.kunde_id = kunde.id
WHERE kunde.name = 'Foede';

-- default INNER JOIN with * selection

SELECT kunde.*, bestellung.id 
FROM kunde
JOIN bestellung ON bestellung.kunde_id = kunde.id
WHERE kunde.vorname = 'Kira';
