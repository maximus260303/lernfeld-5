from django.urls import path

from . import views

urlpatterns = [
    # ex: /polls/
    path('', views.index, name='index'),
    path('<int:recipe_id>/', views.detail, name='detail')
]