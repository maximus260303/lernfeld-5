

# Create your models here.
from django.db import models
import datetime
from django.utils import timezone

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    def __str__(self):
        return self.choice_text

class Adresse(models.Model):
    stadt = models.CharField(max_length=512)
    strasse = models.CharField(max_length=512)
    hausnr = models.CharField(max_length=512)
    plz = models.IntegerField()
    def __str__(self):
        return self.strasse

    class Meta:
        managed = False
        db_table = 'adresse'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Beschraenkung(models.Model):
    name = models.CharField(max_length=512)
    beschreibung = models.CharField(max_length=512)

    class Meta:
        managed = False
        db_table = 'beschraenkung'


class Bestellung(models.Model):
    gesamtpreis = models.FloatField()
    datum = models.DateField()
    kunde = models.ForeignKey('Kunde', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'bestellung'


class BestellungRezept(models.Model):
    bestellung = models.OneToOneField(Bestellung, models.DO_NOTHING, primary_key=True)
    rezept = models.ForeignKey('Rezept', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'bestellung_rezept'
        unique_together = (('bestellung', 'rezept'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Ernaehrungskategorie(models.Model):
    name = models.CharField(max_length=512)
    beschreibung = models.CharField(max_length=512)

    class Meta:
        managed = False
        db_table = 'ernaehrungskategorie'


class Kunde(models.Model):
    name = models.CharField(max_length=512)
    vorname = models.CharField(max_length=512)
    geburtsdatum = models.DateField()
    telefon = models.CharField(max_length=512)
    email = models.CharField(max_length=512)
    adresse = models.ForeignKey(Adresse, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'kunde'


class Lieferant(models.Model):
    name = models.CharField(max_length=512)
    telefon = models.CharField(max_length=512)
    email = models.CharField(max_length=512)
    adresse = models.ForeignKey(Adresse, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'lieferant'


class LieferantZutat(models.Model):
    menge_pro_woche = models.FloatField()
    lieferant = models.OneToOneField(Lieferant, models.DO_NOTHING, primary_key=True)
    zutat = models.ForeignKey('Zutat', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'lieferant_zutat'
        unique_together = (('lieferant', 'zutat'),)


class Mengeneinheit(models.Model):
    name = models.CharField(max_length=512)

    class Meta:
        managed = False
        db_table = 'mengeneinheit'


class PollsChoice(models.Model):
    id = models.BigAutoField(primary_key=True)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField()
    question = models.ForeignKey('PollsQuestion', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'polls_choice'


class PollsQuestion(models.Model):
    id = models.BigAutoField(primary_key=True)
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'polls_question'


class Rezept(models.Model):
    name = models.CharField(max_length=512)
    beschreibung = models.CharField(max_length=512)
    zubereitung = models.CharField(max_length=512)

    class Meta:
        managed = False
        db_table = 'rezept'

    def get_zutat(self):
        instanz = RezeptZutat.zutat()

        if instanz in Rezept:
            return instanz





class RezeptErnaehrungskategorie(models.Model):
    rezept = models.OneToOneField(Rezept, models.DO_NOTHING, primary_key=True)
    ernaehrungskategorie = models.ForeignKey(Ernaehrungskategorie, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'rezept_ernaehrungskategorie'
        unique_together = (('rezept', 'ernaehrungskategorie'),)


class RezeptZutat(models.Model):
    menge = models.PositiveIntegerField()
    rezept = models.OneToOneField(Rezept, models.DO_NOTHING, primary_key=True)
    zutat = models.ForeignKey('Zutat', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'rezept_zutat'
        unique_together = (('rezept', 'zutat'),)


class Zutat(models.Model):
    bestand = models.PositiveIntegerField()
    nettopreis = models.FloatField()
    name = models.CharField(max_length=512)
    mengeneinheit = models.ForeignKey(Mengeneinheit, models.DO_NOTHING)
    kalorien = models.FloatField()
    proteine = models.FloatField()
    kohlenhydrate = models.FloatField()
    zucker = models.FloatField()
    fett = models.FloatField()
    gesaettigte_fettsaeuren = models.FloatField()
    ballaststoffe = models.FloatField()
    natrium = models.FloatField()

    class Meta:
        managed = False
        db_table = 'zutat'


class ZutatBeschraenkung(models.Model):
    zutat = models.ForeignKey(Zutat, models.DO_NOTHING)
    beschraenkung = models.OneToOneField(Beschraenkung, models.DO_NOTHING, primary_key=True)

    class Meta:
        managed = False
        db_table = 'zutat_beschraenkung'
        unique_together = (('beschraenkung', 'zutat'),)
