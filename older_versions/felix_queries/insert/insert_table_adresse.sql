INSERT INTO Adresse(Strasse, HausNr, PLZ, Stadt, FK_Kunde, FK_Lieferant) VALUES 
('Eppendorfer Landstrasse', '104', '20249','Hamburg',1 ,1),
('Ohmstraße', '23', '22765','Hamburg',2 ,1),
('Bilser Berg', '6', '20459','Hamburg',3 ,1),
('Alter Teichweg', '95', '22049','Hamburg',4 ,2),
('Stübels', '10', '22935','Barsbüttel',5 ,2),
('Grotelertwiete', '4a', '21075','Hamburg',6 ,2),
('Küstersweg', '3', '21079','Hamburg',7 ,3),
('Neugrabener Bahnhofstraße', '30', '21149','Hamburg',8 ,3),
('Elbchaussee', '228', '22605','Hamburg',9 ,3);